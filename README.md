# Telegram Richard Matthew Stallman Bot
A Telegram Bot that acts like RMS and is inspired by the [Stallman-Bot](https://github.com/interwho/stallman-bot). Featuring interjections powered by the wisdom of the great rms.

This Project is moved to [TgTriggerBots](https://gitlab.com/BergiuTelegram/TgTriggerBots).

## Dependencies
```shell
sudo apt install openjdk-8-jdk
```

## Installation
```shell
git clone https://gitlab.com/BergiuTelegram/TgRMSBot && cd TelegramRMSBot
./config.sh
./build
```

## Configuration
- Create a new TelegramBot with the [Botfather](https://telegram.me/botfather)
- Disable privacy settings for the bot
- Write your Botname into the file `BOTNAME` and your Token into the file `TOKEN`

## Run
- `./run`

